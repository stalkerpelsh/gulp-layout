'use strict';

var gulp = require('gulp'),
  watch = require('gulp-watch'),
  prefixer = require('gulp-autoprefixer'),
  uglify = require('gulp-uglify'),
  sass = require('gulp-sass'),
  plumber = require('gulp-plumber'),
  sourcemaps = require('gulp-sourcemaps'),
  rigger = require('gulp-rigger'),
  jade = require('gulp-jade'),
  cssmin = require('gulp-minify-css'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  rimraf = require('rimraf'),
  cssmin = require('gulp-minify-css'),
  bower = require('gulp-bower'),
  browserSync = require("browser-sync"),
  path = require('path'),
  GulpSSH = require('gulp-ssh'),
  shell = require('gulp-shell'),
  moment = require('moment'),
  uncss = require('gulp-uncss'),
  critical = require('critical').stream,
  rev = require('gulp-rev-append'),
  reload = browserSync.reload;

var path = {
  build: {
    html: 'build/',
    js: 'build/assets/js/',
    css: 'build/assets/css/',
    img: 'build/assets/img/',
    fonts: 'build/assets/fonts/'
  },
  src: {
    html: 'src/*.jade',
    js: 'src/js/main.js',
    style: 'src/style/main.scss',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*',
    bowerDir: './bower_components' 
  },
  watch: {
    html: 'src/**/*.jade',
    js: 'src/js/**/*.js',
    style: 'src/style/**/*.scss',
    img: 'src/img/**/*.*',
    video: 'src/video/**/*.*',
    mail: 'src/mail/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  clean: './build'
};

var configDeploy = {
  host: '',
  port: 22,
  username: '',
  password: ''
}

var archiveName = 'site.zip';
var timestamp = moment().format('YYYYMMDDHHmmssSSS');
var buildPath = './prod';
var rootPath = '/home/';
var releasesPath = '/home/';
var sitePath = ''
var releasePath = '/home/';

var configLocal = {
  server: {
    baseDir: "./build"
  },
  tunnel: false,
  host: 'localhost',
  port: 9000,
  logPrefix: "YourSite"
};

var gulpSSH = new GulpSSH({
  ignoreErrors: false,
  sshConfig: configDeploy
});

gulp.task('deploy:compress', ['prod'], shell.task("zip " + "-r " + archiveName + " prod"));

gulp.task('deploy:clean', ['deploy:upload'], function() {
  return gulpSSH.exec("rm " + "-rf " + releasePath + "/" + "prod/");
});

gulp.task('deploy:copy', ['deploy:uncompress'], function() {
  return gulpSSH.exec("sh " + releasePath + "/" + "copy.sh");
});

gulp.task('deploy:upload', ['deploy:compress'], function() {
  return gulp.src(archiveName)
    .pipe(gulpSSH.sftp('write', releasePath + '/' + archiveName))
});

gulp.task('deploy:uncompress', ['deploy:clean'], function() {
  return gulpSSH.exec("cd " + releasePath + " && unzip " + archiveName + ' && rm ' + archiveName);
});

gulp.task('deploy', ['prod', 'deploy:compress',
  'deploy:upload',
  'deploy:clean',
  'deploy:uncompress',
  'deploy:copy'
]);

gulp.task('webserver', function() {
  browserSync(configLocal);
});

gulp.task('clean', function(cb) {
  rimraf(path.clean, cb);
});

gulp.task('html:build', function() {
  gulp.src(path.src.html)
    .pipe(jade())
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({
      stream: true
    }));
});

gulp.task('js:build', function() {
  gulp.src(path.src.js)
    .pipe(rigger())
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({
      stream: true
    }));
});

gulp.task('bower', function() { 
  return bower() .pipe(gulp.dest(path.src.bowerDir)) 
});

gulp.task('style:build', function() {
  gulp.src(path.src.style)
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: ['src/style/', path.src.bowerDir + '/bootstrap-sass/assets/stylesheets'],
      outputStyle: 'nested',
      sourceMap: true,
      errLogToConsole: true
    }))
    .pipe(prefixer())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({
      stream: true
    }));
});

gulp.task('image:build', function() {
  gulp.src(path.src.img)
    .pipe(plumber())
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()],
      interlaced: true
    }))
    .pipe(gulp.dest(path.build.img))
});

gulp.task('fonts:build', function() {
  gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
  'html:build',
  'js:build',
  'style:build',
  'fonts:build',
  'image:build'
]);


gulp.task('watch', function() {
  watch([path.watch.html], function(event, cb) {
    gulp.start('html:build');
  });
  watch([path.watch.style], function(event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function(event, cb) {
    gulp.start('js:build');
  });
  watch([path.watch.img], function(event, cb) {
    gulp.start('image:build');
  });
  watch([path.watch.fonts], function(event, cb) {
    gulp.start('fonts:build');
  });
});

gulp.task('prod', ['prod:copy', 'prod:css', 'prod:critical', 'prod:rev']);

gulp.task('prod:copy', function() {
  gulp.src('build/**/*.*')
    .pipe(gulp.dest('prod'));
});
gulp.task('prod:clean', function(cb) {
  rimraf('./prod', cb);
});
gulp.task('prod:css', ['prod:copy'], function() {
  return gulp.src('prod/css/main.css')
    .pipe(uncss({
      html: ['prod/*.html'],
      ignore: [
        // Bootstrap selectors added via JS
        /\w\.in/,
        ".fade",
        ".collapse",
        ".collapsing",
        /(#|\.)navbar(\-[a-zA-Z]+)?/,
        /(#|\.)dropdown(\-[a-zA-Z]+)?/,
        /(#|\.)(open)/,
        /(#|\.)carousel(\-[a-zA-Z]+)?/,
        ".carousel-inner > .next",
        ".carousel-inner > .prev",
        ".carousel-inner > .next",
        ".carousel-inner > .prev",
        ".carousel-inner > .next.left",
        ".carousel-inner > .prev.right",
        ".carousel-inner > .active.left",
        ".carousel-inner > .active.right",
        ".carousel-inner>.item.active",
        ".carousel-inner>.item.next.left",
        ".carousel-inner>.item.prev.right",
        ".bs.carousel",
        ".slid.bs.carousel",
        ".slide.bs.carousel",
        ".modal",
        ".modal.fade.in",
        ".modal-dialog",
        ".modal-document",
        ".modal-scrollbar-measure",
        ".modal-backdrop.fade",
        ".modal-backdrop.in",
        ".modal.fade.modal-dialog",
        ".modal.in.modal-dialog",
        ".modal-open",
        ".in",
        ".modal-backdrop"
        // Custom selectors added via JS
      ]
    }))
    .pipe(cssmin())
    .pipe(gulp.dest('prod/css'));
});

gulp.task('prod:critical', ['prod:css'], function () {
    return gulp.src('prod/*.html')
        .pipe(critical({base: 'prod/', minify: true, inline: true, css: ['prod/css/main.css']}))
        .pipe(gulp.dest('prod'));
});

gulp.task('prod:rev', ['prod:critical'], function() {
  gulp.src('./prod/*.html')
    .pipe(rev())
    .pipe(gulp.dest('./prod'));
});

gulp.task('default', ['bower', 'build', 'webserver', 'watch']);
